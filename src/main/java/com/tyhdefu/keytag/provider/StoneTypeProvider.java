package com.tyhdefu.keytag.provider;

import com.tyhdefu.keytag.api.KeyTagKeys;
import com.tyhdefu.keytag.api.KeyTagRegistryTypes;
import com.tyhdefu.keytag.api.data.stone.StoneType;
import com.tyhdefu.keytag.api.data.stone.StoneTypes;
import com.tyhdefu.keytag.api.groups.BlockItemGroups;
import com.tyhdefu.keytag.api.groups.Group;
import com.tyhdefu.keytag.group.Grouping;
import com.tyhdefu.keytag.provider.common.AbstractImmutableBlockItemKeyTagDataProvider;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.registry.Registry;
import org.spongepowered.api.registry.RegistryTypes;

import java.util.HashMap;
import java.util.Map;

public class StoneTypeProvider extends AbstractImmutableBlockItemKeyTagDataProvider<StoneType> {

    protected StoneTypeProvider() {
        super(KeyTagKeys.STONE_TYPE);
    }

    @Override
    protected void init() {
        Map<ResourceKey, StoneType> regular = new HashMap<>();
        regular.put(ItemTypes.STONE.location(), StoneTypes.STONE.get());
        regular.put(ItemTypes.ANDESITE.location(), StoneTypes.ANDESITE.get());
        regular.put(ItemTypes.BASALT.location(), StoneTypes.BASALT.get());
        regular.put(ItemTypes.BLACKSTONE.location(), StoneTypes.BLACKSTONE.get());
        regular.put(ItemTypes.DIORITE.location(), StoneTypes.DIORITE.get());
        regular.put(ItemTypes.GRANITE.location(), StoneTypes.GRANITE.get());

        addBlockGroup(generateBlockGrouping(BlockItemGroups.STONE.block().get(), regular));
        addItemGroup(generateItemGrouping(BlockItemGroups.STONE.item().get(), regular));

        Map<ResourceKey, StoneType> polished = new HashMap<>();
        polished.put(ItemTypes.POLISHED_ANDESITE.location(), StoneTypes.ANDESITE.get());
        polished.put(ItemTypes.POLISHED_BASALT.location(), StoneTypes.BASALT.get());
        polished.put(ItemTypes.POLISHED_BLACKSTONE.location(), StoneTypes.BLACKSTONE.get());
        polished.put(ItemTypes.POLISHED_DIORITE.location(), StoneTypes.DIORITE.get());
        polished.put(ItemTypes.POLISHED_GRANITE.location(), StoneTypes.GRANITE.get());

        addBlockGroup(generateBlockGrouping(BlockItemGroups.POLISHED_STONE.block().get(), polished));
        addItemGroup(generateItemGrouping(BlockItemGroups.POLISHED_STONE.item().get(), polished));
    }

    private static Grouping<ItemType, StoneType> generateItemGrouping(Group<ItemType, StoneType> group, Map<ResourceKey, StoneType> map) {
        Grouping.Builder<ItemType, StoneType> builder = Grouping.builder();
        builder.id(group.id())
                .registry(KeyTagRegistryTypes.ITEM_TYPE_GROUPS);

        Registry<ItemType> itemTypeRegistry = Sponge.server().registries().registry(RegistryTypes.ITEM_TYPE);
        for (Map.Entry<ResourceKey, StoneType> entry : map.entrySet()) {
            ItemType itemType = itemTypeRegistry.value(entry.getKey());
            builder.add(entry.getValue(), itemType);
        }
        return builder.build();
    }

    private static Grouping<BlockType, StoneType> generateBlockGrouping(Group<BlockType, StoneType> group, Map<ResourceKey, StoneType> map) {
        Grouping.Builder<BlockType, StoneType> builder = Grouping.builder();
        builder.id(group.id())
                .registry(KeyTagRegistryTypes.BLOCK_TYPE_GROUPS);

        Registry<BlockType> blockTypeRegistry = Sponge.server().registries().registry(RegistryTypes.BLOCK_TYPE);
        for (Map.Entry<ResourceKey, StoneType> entry : map.entrySet()) {
            BlockType blockType = blockTypeRegistry.value(entry.getKey());
            builder.add(entry.getValue(), blockType);
        }
        return builder.build();
    }
}
