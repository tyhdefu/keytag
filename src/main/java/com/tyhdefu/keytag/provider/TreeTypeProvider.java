package com.tyhdefu.keytag.provider;

import com.tyhdefu.keytag.api.KeyTagKeys;
import com.tyhdefu.keytag.api.KeyTagRegistryTypes;
import com.tyhdefu.keytag.api.groups.BlockItemGroups;
import com.tyhdefu.keytag.api.groups.BlockTypeGroups;
import com.tyhdefu.keytag.api.groups.DefaultedBlockItemGroupKey;
import com.tyhdefu.keytag.api.groups.Group;
import com.tyhdefu.keytag.api.data.tree.TreeType;
import com.tyhdefu.keytag.api.data.tree.TreeTypes;
import com.tyhdefu.keytag.group.Grouping;
import com.tyhdefu.keytag.group.GroupingMap;
import com.tyhdefu.keytag.provider.common.AbstractImmutableBlockItemKeyTagDataProvider;
import com.tyhdefu.keytag.provider.common.AbstractImmutableKeyTagDataProvider;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.event.lifecycle.RegisterRegistryValueEvent;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.registry.DefaultedRegistryReference;
import org.spongepowered.api.registry.DefaultedRegistryType;
import org.spongepowered.api.registry.RegistryType;
import org.spongepowered.api.registry.RegistryTypes;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

public class TreeTypeProvider extends AbstractImmutableBlockItemKeyTagDataProvider<TreeType> {

    public TreeTypeProvider() {
        super(KeyTagKeys.TREE_TYPE);
    }

    @Override
    protected void init() {
        Set<ResourceKey> hyphaeIgnore = new HashSet<>();
        hyphaeIgnore.add(TreeTypes.CRIMSON.location());
        hyphaeIgnore.add(TreeTypes.WARPED.location());

        this.addBlockGroup(generateBuilderFor(BlockItemGroups.LOG.block().location(), KeyTagRegistryTypes.BLOCK_TYPE_GROUPS,
                RegistryTypes.BLOCK_TYPE, "log", hyphaeIgnore)
                .add(TreeTypes.CRIMSON, BlockTypes.CRIMSON_STEM)
                .add(TreeTypes.WARPED, BlockTypes.WARPED_STEM)
                .build());

        this.addItemGroup(generateBuilderFor(BlockItemGroups.LOG.item().location(), KeyTagRegistryTypes.ITEM_TYPE_GROUPS,
                RegistryTypes.ITEM_TYPE, "log", hyphaeIgnore)
                .add(TreeTypes.CRIMSON, ItemTypes.CRIMSON_STEM)
                .add(TreeTypes.WARPED, ItemTypes.WARPED_STEM)
                .build());

        this.addBlockGroup(generateBuilderFor(BlockItemGroups.STRIPPED_LOG.block().location(), KeyTagRegistryTypes.BLOCK_TYPE_GROUPS,
                RegistryTypes.BLOCK_TYPE, "stripped_log", hyphaeIgnore)
                .add(TreeTypes.CRIMSON, BlockTypes.STRIPPED_CRIMSON_STEM)
                .add(TreeTypes.WARPED, BlockTypes.STRIPPED_WARPED_STEM)
                .build());

        this.addItemGroup(generateBuilderFor(BlockItemGroups.STRIPPED_LOG.item().location(), KeyTagRegistryTypes.ITEM_TYPE_GROUPS,
                RegistryTypes.ITEM_TYPE, "stripped_log", hyphaeIgnore)
                .add(TreeTypes.CRIMSON, ItemTypes.STRIPPED_CRIMSON_STEM)
                .add(TreeTypes.WARPED, ItemTypes.STRIPPED_WARPED_STEM)
                .build());

        this.addBlockGroup(generateBuilderFor(BlockItemGroups.SIX_SIDED_LOG.block().location(), KeyTagRegistryTypes.BLOCK_TYPE_GROUPS,
                RegistryTypes.BLOCK_TYPE, "wood", hyphaeIgnore)
                .add(TreeTypes.CRIMSON, BlockTypes.CRIMSON_HYPHAE)
                .add(TreeTypes.WARPED, BlockTypes.WARPED_HYPHAE)
                .build());

        this.addItemGroup(generateBuilderFor(BlockItemGroups.SIX_SIDED_LOG.item().location(), KeyTagRegistryTypes.ITEM_TYPE_GROUPS,
                RegistryTypes.ITEM_TYPE, "wood", hyphaeIgnore)
                .add(TreeTypes.CRIMSON, ItemTypes.CRIMSON_HYPHAE)
                .add(TreeTypes.WARPED, ItemTypes.WARPED_HYPHAE)
                .build());

        this.addBlockGroup(generateBuilderFor(BlockItemGroups.STRIPPED_SIX_SIDED_LOG.block().location(), KeyTagRegistryTypes.BLOCK_TYPE_GROUPS,
                RegistryTypes.BLOCK_TYPE, "stripped_wood", hyphaeIgnore)
                .add(TreeTypes.CRIMSON, BlockTypes.STRIPPED_CRIMSON_HYPHAE)
                .add(TreeTypes.WARPED, BlockTypes.STRIPPED_WARPED_HYPHAE)
                .build());

        this.addItemGroup(generateBuilderFor(BlockItemGroups.STRIPPED_SIX_SIDED_LOG.item().location(), KeyTagRegistryTypes.ITEM_TYPE_GROUPS,
                RegistryTypes.ITEM_TYPE, "stripped_wood", hyphaeIgnore)
                .add(TreeTypes.CRIMSON, ItemTypes.STRIPPED_CRIMSON_HYPHAE)
                .add(TreeTypes.WARPED, ItemTypes.STRIPPED_WARPED_HYPHAE)
                .build());

        addGeneratedBlockAndItemTypes(BlockItemGroups.BOAT, "boat", hyphaeIgnore);
        addGeneratedBlockAndItemTypes(BlockItemGroups.BUTTON, "button");
        addGeneratedBlockAndItemTypes(BlockItemGroups.DOOR, "door");
        addGeneratedBlockAndItemTypes(BlockItemGroups.FENCE, "fence");
        addGeneratedBlockAndItemTypes(BlockItemGroups.FENCE_GATE, "fence_gate");
        addGeneratedBlockAndItemTypes(BlockItemGroups.LEAVES, "leaves", hyphaeIgnore);
        addGeneratedBlockAndItemTypes(BlockItemGroups.PLANK, "plank");
        addGeneratedBlockAndItemTypes(BlockItemGroups.PRESSURE_PLATE, "pressure_plate");
        addGeneratedBlockAndItemTypes(BlockItemGroups.SAPLING, "sapling");
        addGeneratedBlockAndItemTypes(BlockItemGroups.SIGN, "sign");
        addGeneratedBlockAndItemTypes(BlockItemGroups.SLAB, "slab");
        addGeneratedBlockAndItemTypes(BlockItemGroups.STAIRS, "stairs");
        addGeneratedBlockAndItemTypes(BlockItemGroups.TRAPDOOR, "trapdoor");

        addGeneratedBlockTypes(BlockTypeGroups.WALL_SIGN, "wall_sign");

        addBlockGroup(generateBuilderFor(BlockTypeGroups.POTTED_SAPLING.location(), KeyTagRegistryTypes.BLOCK_TYPE_GROUPS,
                RegistryTypes.BLOCK_TYPE, s -> "potted_" + s + "_sapling", Collections.emptySet())
                .build());
    }

    public void addGeneratedBlockAndItemTypes(final DefaultedBlockItemGroupKey<TreeType> key, final String id) {
        addGeneratedBlockAndItemTypes(key, id, Collections.emptySet());
    }

    public void addGeneratedBlockAndItemTypes(final DefaultedBlockItemGroupKey<TreeType> key, final String id, Set<ResourceKey> ignore) {
        addGeneratedBlockTypes(key.block(), id, ignore);
        addGeneratedItemTypes(key.item(), id, ignore);
    }

    public void addGeneratedBlockTypes(final DefaultedRegistryReference<Group<BlockType, TreeType>> group,
                                       final String blockId) {
        addGeneratedBlockTypes(group, blockId, Collections.emptySet());
    }

    public void addGeneratedBlockTypes(final DefaultedRegistryReference<Group<BlockType, TreeType>> group,
                                       final String blockId, Set<ResourceKey> ignore) {
        this.addBlockGroup(generateBuilderFor(group.location(), KeyTagRegistryTypes.BLOCK_TYPE_GROUPS, RegistryTypes.BLOCK_TYPE, blockId, ignore).build());
    }

    public void addGeneratedItemTypes(final DefaultedRegistryReference<Group<ItemType, TreeType>> group,
                                      final String blockId) {
        addGeneratedItemTypes(group, blockId, Collections.emptySet());
    }

    public void addGeneratedItemTypes(final DefaultedRegistryReference<Group<ItemType, TreeType>> group,
                                      final String blockId, Set<ResourceKey> ignore) {
        this.addItemGroup(generateBuilderFor(group.location(), KeyTagRegistryTypes.ITEM_TYPE_GROUPS, RegistryTypes.ITEM_TYPE, blockId, ignore).build());
    }

    public static <H extends DataHolder> Grouping.Builder<H, TreeType> generateBuilderFor(final ResourceKey groupingId, RegistryType<Group<H, ?>> groupRegistry, DefaultedRegistryType<H> reference,
                                                                                          final Function<String, String> idTransformer, Set<ResourceKey> ignoreTypes) {
        Grouping.Builder<H, TreeType> builder = Grouping.<H, TreeType>builder()
                .id(groupingId)
                .registry(groupRegistry);

        KeyTagRegistryTypes.TREE_TYPE.get().streamEntries()
                .filter(entry -> entry.key().namespace().equals(ResourceKey.SPONGE_NAMESPACE))
                .filter(entry -> !ignoreTypes.contains(entry.key()))
                .forEach(entry -> {
                    final String id = idTransformer.apply(entry.key().value());
                    H type = reference.get().findValue(ResourceKey.minecraft(id))
                            .orElseThrow(() -> new IllegalStateException("Failed to locate " + id + " with tree type " + entry.key()));
                    builder.add(entry.value(), type);
                });

        return builder;
    }

    public static <H extends DataHolder> Grouping.Builder<H, TreeType> generateBuilderFor(final ResourceKey groupingId, RegistryType<Group<H, ?>> groupRegistry,
                                                                                          DefaultedRegistryType<H> reference, final String id, Set<ResourceKey> ignoreTypes) {
        return generateBuilderFor(groupingId, groupRegistry, reference, s -> s + "_" + id, ignoreTypes);
    }
}
