package com.tyhdefu.keytag.provider;

import com.tyhdefu.keytag.api.KeyTagKeys;
import com.tyhdefu.keytag.api.KeyTagRegistryTypes;
import com.tyhdefu.keytag.api.groups.BlockItemGroups;
import com.tyhdefu.keytag.api.groups.BlockTypeGroups;
import com.tyhdefu.keytag.api.groups.DefaultedBlockItemGroupKey;
import com.tyhdefu.keytag.api.groups.Group;
import com.tyhdefu.keytag.api.groups.ItemTypeGroups;
import com.tyhdefu.keytag.group.Grouping;
import com.tyhdefu.keytag.group.GroupingMap;
import com.tyhdefu.keytag.provider.common.AbstractImmutableBlockItemKeyTagDataProvider;
import com.tyhdefu.keytag.provider.common.AbstractImmutableKeyTagDataProvider;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.event.lifecycle.RegisterRegistryValueEvent;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.registry.DefaultedRegistryType;
import org.spongepowered.api.registry.RegistryType;
import org.spongepowered.api.registry.RegistryTypes;

import java.util.Optional;

public final class DyeColorProvider extends AbstractImmutableBlockItemKeyTagDataProvider<DyeColor> {

    public DyeColorProvider() {
        super(KeyTagKeys.DYE_COLOR);
    }

    @Override
    protected void init() {
        addGeneratedBlockAndItem(BlockItemGroups.BANNER, "banner");
        addGeneratedBlockAndItem(BlockItemGroups.BED, "bed");
        addGeneratedBlockAndItem(BlockItemGroups.CARPET, "carpet");
        addGeneratedBlockAndItem(BlockItemGroups.CONCRETE, "concrete");
        addGeneratedBlockAndItem(BlockItemGroups.CONCRETE_POWDER, "concrete_powder");
        addGeneratedBlockAndItem(BlockItemGroups.GLAZED_TERRACOTTA, "glazed_terracotta");
        addGeneratedBlockAndItem(BlockItemGroups.SHULKER_BOX, "shulker_box");
        addGeneratedBlockAndItem(BlockItemGroups.STAINED_GLASS, "stained_glass");
        addGeneratedBlockAndItem(BlockItemGroups.STAINED_GLASS_PANE, "stained_glass_pane");
        addGeneratedBlockAndItem(BlockItemGroups.WOOL, "wool");
        addGeneratedBlockAndItem(BlockItemGroups.TERRACOTTA, "terracotta");

        addGeneratedBlock(BlockTypeGroups.WALL_BANNER.location(), "wall_banner");

        addGeneratedItem(ItemTypeGroups.DYE.location(), "dye");

        Grouping<BlockType, DyeColor> tulipBlock = Grouping.builder(BlockType.class, DyeColor.class)
                .id(BlockItemGroups.TULIP.block().location())
                .registry(KeyTagRegistryTypes.BLOCK_TYPE_GROUPS)
                .add(DyeColors.WHITE, BlockTypes.WHITE_TULIP)
                .add(DyeColors.ORANGE, BlockTypes.ORANGE_TULIP)
                .add(DyeColors.PINK, BlockTypes.PINK_TULIP)
                .add(DyeColors.RED, BlockTypes.RED_TULIP)
                .build();

        addBlockGroup(tulipBlock);

        Grouping<BlockType, DyeColor> pottedTulip = Grouping.builder(BlockType.class, DyeColor.class)
                .id(BlockTypeGroups.POTTED_TULIP.location())
                .registry(KeyTagRegistryTypes.BLOCK_TYPE_GROUPS)
                .add(DyeColors.WHITE, BlockTypes.POTTED_WHITE_TULIP)
                .add(DyeColors.ORANGE, BlockTypes.POTTED_ORANGE_TULIP)
                .add(DyeColors.PINK, BlockTypes.POTTED_PINK_TULIP)
                .add(DyeColors.RED, BlockTypes.POTTED_RED_TULIP)
                .build();

        addBlockGroup(pottedTulip);

        Grouping<ItemType, DyeColor> tulipItem = Grouping.builder(ItemType.class, DyeColor.class)
                .id(BlockItemGroups.TULIP.item().location())
                .registry(KeyTagRegistryTypes.ITEM_TYPE_GROUPS)
                .add(DyeColors.WHITE, ItemTypes.WHITE_TULIP)
                .add(DyeColors.ORANGE, ItemTypes.ORANGE_TULIP)
                .add(DyeColors.PINK, ItemTypes.PINK_TULIP)
                .add(DyeColors.RED, ItemTypes.RED_TULIP)
                .build();

        addItemGroup(tulipItem);
    }

    private void addGeneratedBlockAndItem(DefaultedBlockItemGroupKey<DyeColor> key, String id) {
        addGeneratedBlock(key.block().location(), id);
        addGeneratedItem(key.item().location(), id);
    }

    private void addGeneratedBlock(ResourceKey key, String blockId) {
        Grouping<BlockType, DyeColor> grouping = generateBlockTypeGrouping(key, blockId);
        addBlockGroup(grouping);
    }

    private void addGeneratedItem(ResourceKey key, String blockId) {
        addItemGroup(generateItemTypeGrouping(key, blockId));
    }

    private static Grouping<BlockType, DyeColor> generateBlockTypeGrouping(final ResourceKey groupingId, final String blockId) {
        return generateFor(groupingId, KeyTagRegistryTypes.BLOCK_TYPE_GROUPS, RegistryTypes.BLOCK_TYPE, blockId);
    }

    private static Grouping<ItemType, DyeColor> generateItemTypeGrouping(final ResourceKey groupingId, final String blockId) {
        return generateFor(groupingId, KeyTagRegistryTypes.ITEM_TYPE_GROUPS, RegistryTypes.ITEM_TYPE, blockId);
    }

    private static <T extends DataHolder> Grouping<T, DyeColor> generateFor(final ResourceKey groupingId, RegistryType<Group<T, ?>> groupRegistry, DefaultedRegistryType<T> reference, final String blockId) {
        Grouping.Builder<T, DyeColor> builder = Grouping.<T, DyeColor>builder()
                .id(groupingId)
                .registry(groupRegistry);

        RegistryTypes.DYE_COLOR.get().streamEntries()
                .filter(entry -> entry.key().namespace().equals(ResourceKey.SPONGE_NAMESPACE))
                .forEach(entry -> {
                    T type = reference.get().findValue(ResourceKey.minecraft(entry.key().value() + "_" + blockId))
                            .orElseThrow(() -> new IllegalStateException("Failed to locate " + blockId + " with dye color: " + entry.key()));
                    builder.add(entry.value(), type);
                });
        return builder.build();
    }
}
