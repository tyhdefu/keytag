package com.tyhdefu.keytag.provider;

import com.tyhdefu.keytag.api.KeyTagKeys;
import com.tyhdefu.keytag.api.KeyTagRegistryTypes;
import com.tyhdefu.keytag.api.data.material.Material;
import com.tyhdefu.keytag.api.data.material.Materials;
import com.tyhdefu.keytag.api.groups.Group;
import com.tyhdefu.keytag.api.groups.ItemTypeGroups;
import com.tyhdefu.keytag.group.Grouping;
import com.tyhdefu.keytag.group.GroupingMap;
import com.tyhdefu.keytag.provider.common.AbstractImmutableKeyTagDataProvider;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.lifecycle.RegisterRegistryValueEvent;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.registry.RegistryTypes;

public class MaterialProvider extends AbstractImmutableKeyTagDataProvider<ItemType, Material> {

    private final GroupingMap<ItemType, Material> itemTypeGroups = new GroupingMap<>();

    protected MaterialProvider() {
        super(KeyTagKeys.MATERIAL, ItemType.class);
    }

    @Override
    protected void init() {
        this.itemTypeGroups.add(generateToolGrouping(ItemTypeGroups.SWORD.get()));
        this.itemTypeGroups.add(generateToolGrouping(ItemTypeGroups.PICKAXE.get()));
        this.itemTypeGroups.add(generateToolGrouping(ItemTypeGroups.AXE.get()));
        this.itemTypeGroups.add(generateToolGrouping(ItemTypeGroups.SHOVEL.get()));
        this.itemTypeGroups.add(generateToolGrouping(ItemTypeGroups.HOE.get()));

        this.itemTypeGroups.add(generateArmorGrouping(ItemTypeGroups.HELMET.get())
                .add(Materials.TURTLE, ItemTypes.TURTLE_HELMET).build());
        this.itemTypeGroups.add(generateArmorGrouping(ItemTypeGroups.CHESTPLATE.get()).build());
        this.itemTypeGroups.add(generateArmorGrouping(ItemTypeGroups.LEGGINGS.get()).build());
        this.itemTypeGroups.add(generateArmorGrouping(ItemTypeGroups.BOOTS.get()).build());
    }

    public static Grouping<ItemType, Material> generateToolGrouping(Group<ItemType, Material> toolGroup) {
        String toolId = toolGroup.id().value();
        return Grouping.<ItemType, Material>builder()
                .id(toolGroup.id())
                .registry(KeyTagRegistryTypes.ITEM_TYPE_GROUPS)
                .add(Materials.WOOD, getItemType("wooden_" + toolId))
                .add(Materials.STONE, getItemType("stone_" + toolId))
                .add(Materials.IRON, getItemType("iron_" + toolId))
                .add(Materials.GOLD, getItemType("gold_" + toolId))
                .add(Materials.DIAMOND, getItemType("diamond_" + toolId))
                .add(Materials.NETHERITE, getItemType("netherite_" + toolId))
                .build();

    }

    public static Grouping.Builder<ItemType, Material> generateArmorGrouping(Group<ItemType, Material> armorGroup) {
        String armorId = armorGroup.id().value();
        return Grouping.<ItemType, Material>builder()
                .id(armorGroup.id())
                .registry(KeyTagRegistryTypes.ITEM_TYPE_GROUPS)
                .add(Materials.LEATHER, getItemType("leather_" + armorId))
                .add(Materials.IRON, getItemType("iron_" + armorId))
                .add(Materials.GOLD, getItemType("gold_" + armorId))
                .add(Materials.DIAMOND, getItemType("diamond_" + armorId))
                .add(Materials.NETHERITE, getItemType("netherite_" + armorId));
    }

    public static ItemType getItemType(String item) {
        return Sponge.server().registries().registry(RegistryTypes.ITEM_TYPE).value(ResourceKey.minecraft(item));
    }

    @Override
    protected Material get(ItemType dataHolder) {
        return this.itemTypeGroups.getGrouping(dataHolder)
                .flatMap(grouping -> grouping.get(dataHolder))
                .orElse(null);
    }

    @Override
    protected ItemType with(ItemType dataHolder, Material value) {
        return this.itemTypeGroups.getGrouping(dataHolder)
                .flatMap(grouping -> grouping.holderFor(value))
                .orElse(null);
    }

    @Override
    protected boolean supports(ItemType dataHolder) {
        return this.itemTypeGroups.getGrouping(dataHolder).isPresent();
    }

    @Override
    public void registerGroupsInternal(RegisterRegistryValueEvent event) {
        this.ensureInitialised();
        RegisterRegistryValueEvent.RegistryStep<Group<ItemType, ?>> step = event.registry(KeyTagRegistryTypes.ITEM_TYPE_GROUPS);
        this.itemTypeGroups.groupings().forEach(group -> step.register(group.id(), group));
    }
}
