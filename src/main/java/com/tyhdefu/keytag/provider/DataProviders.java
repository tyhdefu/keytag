package com.tyhdefu.keytag.provider;

import com.google.common.collect.ImmutableMap;
import com.tyhdefu.keytag.provider.common.KeyTagDataProvider;
import org.spongepowered.api.data.Key;

import java.util.HashMap;
import java.util.Map;

public class DataProviders {

    private boolean initialized = false;
    private final Map<Key<?>, KeyTagDataProvider<?>> providers = new HashMap<>();

    private void register() {
        add(new DyeColorProvider());
        add(new TreeTypeProvider());
        add(new MaterialProvider());
        add(new StoneTypeProvider());
        this.initialized = true;
    }

    private void add(KeyTagDataProvider<?> provider) {
        this.providers.put(provider.key(), provider);
    }

    public Map<Key<?>, KeyTagDataProvider<?>> getAll() {
        if (!initialized) {
            register();
        }
        return ImmutableMap.copyOf(providers);
    }
}
