package com.tyhdefu.keytag.provider.common;

import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.DataProvider;
import org.spongepowered.api.data.Key;
import org.spongepowered.api.data.value.Value;
import org.spongepowered.api.event.lifecycle.RegisterRegistryValueEvent;

public abstract class AbstractImmutableKeyTagDataProvider<H extends DataHolder, E> extends KeyTagDataProvider<E> {
    private final Class<H> dataHolderClass;
    private boolean initialised = false;

    protected AbstractImmutableKeyTagDataProvider(Key<? extends Value<E>> key, Class<H> dataHolderClass) {
        super(key);
        this.dataHolderClass = dataHolderClass;
    }

    @Override
    public DataProvider<? extends Value<E>, E> create() {
        return DataProvider.immutableBuilder()
                .key(this.key())
                .dataHolder(this.dataHolderClass)
                .get(this::get)
                .set(this::with)
                .supports(this::supports)
                .build();
    }

    public void ensureInitialised() {
        if (!initialised) {
            init();
            initialised = true;
        }
    }

    @Override
    public final void registerGroups(RegisterRegistryValueEvent event) {
        this.ensureInitialised();
        this.registerGroupsInternal(event);
    }

    protected abstract void registerGroupsInternal(RegisterRegistryValueEvent event);

    protected abstract void init();

    protected abstract E get(H dataHolder);

    protected abstract H with(H dataHolder, E value);

    protected abstract boolean supports(H dataHolder);
}
