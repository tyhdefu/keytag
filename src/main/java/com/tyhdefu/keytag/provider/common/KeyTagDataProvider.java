package com.tyhdefu.keytag.provider.common;

import org.spongepowered.api.data.DataProvider;
import org.spongepowered.api.data.Key;
import org.spongepowered.api.data.value.Value;
import org.spongepowered.api.event.lifecycle.RegisterRegistryValueEvent;

public abstract class KeyTagDataProvider<E> {

    private final Key<? extends Value<E>> key;

    protected KeyTagDataProvider(Key<? extends Value<E>> key) {
        this.key = key;
    }

    public Key<? extends Value<E>> key() {
        return key;
    }

    public abstract DataProvider<? extends Value<E>, E> create();

    public abstract void registerGroups(RegisterRegistryValueEvent event);

}
