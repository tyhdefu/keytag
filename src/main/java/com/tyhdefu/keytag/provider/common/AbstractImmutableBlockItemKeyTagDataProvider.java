package com.tyhdefu.keytag.provider.common;

import com.tyhdefu.keytag.api.KeyTagRegistryTypes;
import com.tyhdefu.keytag.api.groups.Group;
import com.tyhdefu.keytag.group.Grouping;
import com.tyhdefu.keytag.group.GroupingMap;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.Key;
import org.spongepowered.api.data.value.Value;
import org.spongepowered.api.event.lifecycle.RegisterRegistryValueEvent;
import org.spongepowered.api.item.ItemType;

public abstract class AbstractImmutableBlockItemKeyTagDataProvider<T> extends AbstractImmutableKeyTagDataProvider<DataHolder, T> {

    private final GroupingMap<BlockType, T> blockTypeGroupings = new GroupingMap<>();
    private final GroupingMap<ItemType, T> itemTypeGroupings = new GroupingMap<>();

    protected AbstractImmutableBlockItemKeyTagDataProvider(Key<? extends Value<T>> key) {
        super(key, DataHolder.class);
    }

    public void addItemGroup(Grouping<ItemType, T> group) {
        this.itemTypeGroupings.add(group);
    }

    public void addBlockGroup(Grouping<BlockType, T> group) {
        this.blockTypeGroupings.add(group);
    }

    @Override
    protected void registerGroupsInternal(RegisterRegistryValueEvent event) {
        this.ensureInitialised();
        RegisterRegistryValueEvent.RegistryStep<Group<ItemType, ?>> itemStep = event.registry(KeyTagRegistryTypes.ITEM_TYPE_GROUPS);
        this.itemTypeGroupings.groupings().forEach(grouping -> itemStep.register(grouping.id(), grouping));

        RegisterRegistryValueEvent.RegistryStep<Group<BlockType, ?>> blockStep = event.registry(KeyTagRegistryTypes.BLOCK_TYPE_GROUPS);
        this.blockTypeGroupings.groupings().forEach(grouping -> blockStep.register(grouping.id(), grouping));
    }

    @Override
    protected T get(DataHolder dataHolder) {
        if (dataHolder instanceof ItemType) {
            ItemType itemType = (ItemType) dataHolder;
            return this.itemTypeGroupings.getValue(itemType).orElse(null);
        }
        else if (dataHolder instanceof BlockType) {
            BlockType blockType = (BlockType) dataHolder;
            return this.blockTypeGroupings.getValue(blockType).orElse(null);
        }
        return null;
    }

    @Override
    protected DataHolder with(DataHolder dataHolder, T value) {
        if (dataHolder instanceof ItemType) {
            ItemType itemType = (ItemType) dataHolder;
            return this.itemTypeGroupings.getGrouping(itemType)
                    .flatMap(grouping -> grouping.holderFor(value))
                    .orElse(null);
        }
        else if (dataHolder instanceof BlockType) {
            BlockType blockType = (BlockType) dataHolder;
            return this.blockTypeGroupings.getGrouping(blockType)
                    .flatMap(grouping -> grouping.holderFor(value))
                    .orElse(null);
        }
        return null;
    }

    @Override
    protected boolean supports(DataHolder dataHolder) {
        if (dataHolder instanceof ItemType) {
            return this.itemTypeGroupings.getGrouping((ItemType) dataHolder).isPresent();
        }
        else if (dataHolder instanceof BlockType) {
            return this.blockTypeGroupings.getGrouping((BlockType) dataHolder).isPresent();
        }
        return false;
    }
}
