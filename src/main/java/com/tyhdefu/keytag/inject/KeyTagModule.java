package com.tyhdefu.keytag.inject;

import com.google.inject.PrivateModule;
import com.tyhdefu.keytag.api.Plugin;

public class KeyTagModule extends PrivateModule {

    private final Plugin pluginInstance;

    public KeyTagModule(Plugin pluginInstance) {
        this.pluginInstance = pluginInstance;
    }

    @Override
    protected void configure() {
        this.bind(Plugin.class).toInstance(pluginInstance);
    }
}
