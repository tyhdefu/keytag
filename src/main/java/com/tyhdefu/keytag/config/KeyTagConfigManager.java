package com.tyhdefu.keytag.config;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import com.tyhdefu.keytag.KeyTagPlugin;
import com.tyhdefu.keytag.api.ConfigManager;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.configurate.CommentedConfigurationNode;
import org.spongepowered.configurate.hocon.HoconConfigurationLoader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class KeyTagConfigManager implements ConfigManager {

    private final Map<ResourceKey, GroupAdditionConfig> groupAdditionConfigsBySource = new HashMap<>();
    private final Multimap<ResourceKey, GroupAdditionConfig> groupAdditionConfigByTarget = HashMultimap.create();

    public void loadGroupAdditions(final Path groupAdditions) {
        final HoconConfigurationLoader loader = HoconConfigurationLoader.builder()
                .path(groupAdditions)
                .build();
        try {
            if (Files.readAllLines(groupAdditions).isEmpty()) {
                KeyTagPlugin.getLogger().info("Filling " + groupAdditions.getFileName() + " with empty config stub.");
                loader.save(CommentedConfigurationNode.root().set(new GroupAdditionConfig()));
                return;
            }
            final GroupAdditionConfig groupAdditionConfig = loader.load().get(GroupAdditionConfig.class);
            if (groupAdditionConfig == null) {
                KeyTagPlugin.getLogger().error("Group addition config " + groupAdditions.getFileName() + " is empty! (" + groupAdditions + ")");
                return;
            }
            this.groupAdditionConfigsBySource.put(groupAdditionConfig.getSource(), groupAdditionConfig);
            this.groupAdditionConfigByTarget.put(groupAdditionConfig.getTarget(), groupAdditionConfig);
        } catch (IOException e) {
            KeyTagPlugin.getLogger().error("Failed to load group additions: " + groupAdditions.getFileName() + " (" + groupAdditions +  ")", e);
        }
    }

    @Override
    public boolean isAdditionConfigApplied(ResourceKey id) {
        return this.groupAdditionConfigsBySource.containsKey(id);
    }

    public Map<ResourceKey, GroupAdditionConfig> getGroupAdditionConfigsBySource() {
        return ImmutableMap.copyOf(this.groupAdditionConfigsBySource);
    }

    public Multimap<ResourceKey, GroupAdditionConfig> getGroupAdditionConfigByTarget() {
        return ImmutableMultimap.copyOf(this.groupAdditionConfigByTarget);
    }
}
