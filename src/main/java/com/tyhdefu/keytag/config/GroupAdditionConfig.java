package com.tyhdefu.keytag.config;

import org.spongepowered.api.ResourceKey;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;
import org.spongepowered.configurate.objectmapping.meta.Comment;

import java.util.Map;

@ConfigSerializable
public class GroupAdditionConfig {

    @Comment("The ResourceKey to identify this addition. Maybe be used by other plugins to ensure their configs have been applied.")
    private ResourceKey source;

    @Comment("The ResourceKey to identify group this addition will target.")
    private ResourceKey target;

    @Comment("The addition to be added to the group")
    private Map<ResourceKey, ResourceKey> additions;

    public ResourceKey getSource() {
        return this.source;
    }

    public ResourceKey getTarget() {
        return this.target;
    }

    public Map<ResourceKey, ResourceKey> getAdditions() {
        return this.additions;
    }
}
