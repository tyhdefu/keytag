package com.tyhdefu.keytag;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.tyhdefu.keytag.api.ConfigManager;
import com.tyhdefu.keytag.api.KeyTagKeys;
import com.tyhdefu.keytag.api.KeyTagRegistryTypes;
import com.tyhdefu.keytag.api.Plugin;
import com.tyhdefu.keytag.config.KeyTagConfigManager;
import com.tyhdefu.keytag.data.KeyTagRegistryLoader;
import com.tyhdefu.keytag.inject.KeyTagModule;
import com.tyhdefu.keytag.provider.DataProviders;
import com.tyhdefu.keytag.provider.common.KeyTagDataProvider;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.apache.logging.log4j.Logger;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.command.Command;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.exception.CommandException;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.data.DataRegistration;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.lifecycle.ConstructPluginEvent;
import org.spongepowered.api.event.lifecycle.RegisterCommandEvent;
import org.spongepowered.api.event.lifecycle.RegisterDataEvent;
import org.spongepowered.api.event.lifecycle.RegisterRegistryEvent;
import org.spongepowered.api.event.lifecycle.RegisterRegistryValueEvent;
import org.spongepowered.plugin.PluginContainer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class KeyTagPlugin implements Plugin {

    private final Logger logger;
    private final PluginContainer pluginContainer;
    private final DataProviders dataProviders = new DataProviders();

    @Inject
    @ConfigDir(sharedRoot = false)
    private Path configDir;

    private final KeyTagConfigManager configManager = new KeyTagConfigManager();

    private static KeyTagPlugin INSTANCE;

    @Inject
    public KeyTagPlugin(final Logger logger, final PluginContainer pluginContainer) {
        this.logger = logger;
        this.pluginContainer = pluginContainer;
        INSTANCE = this;
    }

    @Override
    public ConfigManager config() {
        return this.configManager;
    }

    public static KeyTagPlugin getInstance() {
        return INSTANCE;
    }

    public static Logger getLogger() {
        return getInstance().logger;
    }

    @Listener
    public void constructPlugin(ConstructPluginEvent event) {
        // Inject our Plugin instance to allow api-implementation separation
        Guice.createInjector(new KeyTagModule(this));

        final Path extraGroups = configDir.resolve("ExtraGroups");

        if (Files.exists(extraGroups)) {
            try {
                Files.walk(extraGroups); // TODO
            } catch (IOException e) {
                logger.error("Failed to load any extra groups", e);
            }
        }

        final Path groupAdditions = configDir.resolve("GroupAdditions");

        if (Files.exists(groupAdditions)) {
            try {
                Files.walk(groupAdditions)
                        .filter(Files::isRegularFile)
                        .forEach(configManager::loadGroupAdditions);
            } catch (IOException e) {
                logger.error("Failed to load any group additions config", e);
            }
        }
        final int groupAdditionConfigsLoaded = configManager.getGroupAdditionConfigsBySource().size();
        logger.info("Read " + groupAdditionConfigsLoaded + " group addition config" + ((groupAdditionConfigsLoaded == 1) ? "" : "s"));
    }

    @Listener
    public void registerCommands(RegisterCommandEvent<Command.Parameterized> event) {
        Command.Parameterized test = Command.builder()
                .permission("keytag.test")
                .executor(ctx -> {
                    Audience audience = ctx.cause().audience();
                    BlockType newWool = BlockTypes.RED_WOOL.get().with(KeyTagKeys.DYE_COLOR, DyeColors.PURPLE.get())
                            .orElseThrow(() -> new CommandException(Component.text("Test failed: Red wool .with purple did not produce any value.")));

                    if (newWool != BlockTypes.PURPLE_WOOL.get()) {
                        throw new CommandException(Component.text("Test failed: produced " + newWool + " instead of purple wool"));
                    }
                    audience.sendMessage(Component.text("Successfully transformed a red wool into a purple wool", NamedTextColor.GREEN));
                    return CommandResult.success();
                })
                .build();

        Command.Parameterized main = Command.builder()
                .addChild(test, "test")
                .build();

        event.register(this.pluginContainer, main, "keytag");
    }

    @Listener(order = Order.EARLY)
    public void registerKeys(final RegisterDataEvent event) {
        dataProviders.getAll().values().stream().map(this::createRegistration).forEach(event::register);
    }

    @Listener(order = Order.EARLY)
    public void registerRegistries(final RegisterRegistryEvent event) {
        KeyTagRegistryLoader.registerGame(event);

        event.register(KeyTagRegistryTypes.BLOCK_TYPE_GROUPS.location(), true);
        event.register(KeyTagRegistryTypes.ITEM_TYPE_GROUPS.location(), true);
    }

    @Listener(order = Order.EARLY)
    public void registerRegistryValues(final RegisterRegistryValueEvent event) {
        dataProviders.getAll().values().forEach(provider -> provider.registerGroups(event));
    }

    private <E> DataRegistration createRegistration(KeyTagDataProvider<E> dataProvider) {
        return DataRegistration.builder().dataKey(dataProvider.key()).provider(dataProvider.create()).build();
    }
}
