package com.tyhdefu.keytag.group;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableSet;
import com.tyhdefu.keytag.KeyTagPlugin;
import com.tyhdefu.keytag.api.groups.Group;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.registry.DefaultedRegistryReference;
import org.spongepowered.api.registry.RegistryType;
import org.spongepowered.plugin.PluginContainer;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

public class Grouping<H extends DataHolder, V> implements Group<H, V> {

    public static <H extends DataHolder, V> Builder<H, V> builder() {
        return new Builder<>();
    }

    public static <H extends DataHolder, V> Builder<H, V> builder(Class<H> holderClass, Class<V> valueClazz) {
        return new Builder<>();
    }

    private final ResourceKey id;
    private final RegistryType<Group<H, ?>> registryType;
    private final BiMap<H, V> map;

    public Grouping(final ResourceKey id, RegistryType<Group<H, ?>> registryType, final BiMap<H, V> map) {
        this.id = id;
        this.registryType = registryType;
        this.map = map;
    }

    public ResourceKey id() {
        return id;
    }

    @Override
    public RegistryType<Group<H, ?>> registryType() {
        return this.registryType;
    }

    public boolean contains(H holder) {
        return map.containsKey(holder);
    }

    public V require(H holder) {
        return Objects.requireNonNull(map.get(holder));
    }

    @Override
    public Optional<V> get(H holder) {
        return Optional.ofNullable(map.get(holder));
    }

    public H requireHolderFor(V value) {
        return Objects.requireNonNull(map.inverse().get(value));
    }

    public Optional<H> holderFor(V value) {
        return Optional.ofNullable(map.inverse().get(value));
    }

    public Collection<H> allHolders() {
        return ImmutableSet.copyOf(this.map.keySet());
    }

    @Override
    public void registerAdditional(PluginContainer cause, DefaultedRegistryReference<H> holder, DefaultedRegistryReference<V> value) {
        this.map.put(holder.get(), value.get());
    }

    @Override
    public String toString() {
        return "Grouping{" +
                "id=" + id +
                ", map=" + map +
                '}';
    }

    public static class Builder<H extends DataHolder, V> {
        private ResourceKey id;
        private RegistryType<Group<H, ?>> registryType;
        private final BiMap<H, V> map = HashBiMap.create();

        public Builder<H, V> id(final ResourceKey id) {
            this.id = id;
            return this;
        }

        public Builder<H, V> registry(RegistryType<Group<H, ?>> registryType) {
            this.registryType = registryType;
            return this;
        }

        public Builder<H, V> add(final V value, final DefaultedRegistryReference<H> holder) {
            return this.add(value, holder.get());
        }

        public Builder<H, V> add(final DefaultedRegistryReference<V> value, final H holder) {
            return this.add(value.get(), holder);
        }

        public Builder<H, V> add(final DefaultedRegistryReference<V> value, final DefaultedRegistryReference<H> holder) {
            return this.add(value.get(), holder.get());
        }

        public Builder<H, V> add(final V value, final H holder) {
            this.map.put(holder, value);
            return this;
        }

        public Grouping<H, V> build() {
            if (this.id == null) {
                throw new IllegalStateException("Builder missing id!");
            }
            if (this.registryType == null) {
                throw new IllegalStateException("Builder missing registry type");
            }
            return new Grouping<>(this.id, registryType, ImmutableBiMap.copyOf(this.map));
        }
    }
}
