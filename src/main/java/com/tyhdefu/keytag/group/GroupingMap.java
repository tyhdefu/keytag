package com.tyhdefu.keytag.group;

import com.google.common.collect.ImmutableSet;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.data.DataHolder;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class GroupingMap<H extends DataHolder, V> {

    public static <H extends DataHolder, V> Builder<H, V> builder(Class<H> holderClazz, Class<V> valueClazz) {
        return new Builder<>();
    }

    private final Map<H, Grouping<H, V>> map = new HashMap<>();

    public GroupingMap<H, V> add(final Grouping<H, V> grouping) {
        for (H holder : grouping.allHolders()) {
            this.map.put(holder, grouping);
        }
        return this;
    }

    public Optional<Grouping<H, V>> getGrouping(H holder) {
        return Optional.ofNullable(map.get(holder));
    }

    public Optional<V> getValue(H holder) {
        return this.getGrouping(holder).flatMap(grouping -> grouping.get(holder));
    }

    public Collection<Grouping<H, V>> groupings() {
        return ImmutableSet.copyOf(this.map.values());
    }

    public static class Builder<H extends DataHolder, V> {
        private final Map<ResourceKey, Grouping.Builder<H, V>> immutableGroups = new HashMap<>();
        private final Map<DataHolder, ResourceKey> map = new HashMap<>();
        private final Set<DataHolder> assignedHolders = new HashSet<>();

        public Builder<H, V> addGroup(final ResourceKey id, Iterable<H> holders) {
            for (DataHolder holder : holders) {
                this.map.put(holder, id);
            }
            return this;
        }

        public Builder<H, V> assign(V value, Iterable<H> holders) {
            for (H holder : holders) {
                // Find which set it belongs to.
                final ResourceKey belongedSet = map.get(holder);
                if (belongedSet == null) {
                    throw new IllegalStateException("Tried to assign " + holder + " the value " + value + " but it had no grouping!");
                }
                Grouping.Builder<H, V> builder =  this.immutableGroups.computeIfAbsent(belongedSet, a -> Grouping.builder());
                builder.add(value, holder);
                this.assignedHolders.add(holder);
            }
            return this;
        }

        public GroupingMap<H, V> build() {
            if (assignedHolders.equals(map.keySet())) {
                throw new IllegalStateException("Not all holders were assigned! Assigned: " + assignedHolders + " vs expected to be assigned: " + map.keySet());
            }
            final GroupingMap<H, V> immutableGroupingMap = new GroupingMap<>();
            immutableGroups.forEach((k, v) -> immutableGroupingMap.add(v.build()));
            return immutableGroupingMap;
        }
    }
}
