package com.tyhdefu.keytag.data;

import com.google.common.collect.ImmutableMap;
import com.tyhdefu.keytag.api.KeyTagConstants;
import com.tyhdefu.keytag.api.KeyTagRegistryTypes;
import com.tyhdefu.keytag.api.data.material.Material;
import com.tyhdefu.keytag.api.data.material.Materials;
import com.tyhdefu.keytag.api.data.tree.TreeType;
import com.tyhdefu.keytag.api.data.tree.TreeTypes;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.event.lifecycle.RegisterRegistryEvent;
import org.spongepowered.api.registry.DefaultedRegistryReference;

import java.util.HashMap;
import java.util.Map;

public class KeyTagRegistryLoader {

    public static void registerGame(RegisterRegistryEvent event) {
        event.register(KeyTagRegistryTypes.TREE_TYPE.location(), true, KeyTagRegistryLoader::treeTypes);
        event.register(KeyTagRegistryTypes.MATERIAL.location(), true, KeyTagRegistryLoader::materials);
    }

    public static ResourceKey key(final String id) {
        return ResourceKey.of(KeyTagConstants.PLUGIN_ID, id);
    }

    public static <T> LoaderBuilder<T> loaderBuilder(Class<T> clazz) {
        return new LoaderBuilder<>();
    }

    public static class LoaderBuilder<T> {
        private final Map<ResourceKey, T> map = new HashMap<>();

        public LoaderBuilder<T> add(DefaultedRegistryReference<T> reference, T value) {
            this.map.put(reference.location(), value);
            return this;
        }

        public Map<ResourceKey, T> build() {
            return ImmutableMap.copyOf(map);
        }
    }

    public static Map<ResourceKey, TreeType> treeTypes() {
        return loaderBuilder(TreeType.class)
                .add(TreeTypes.ACACIA, new KeyTagTreeSpecies())
                .add(TreeTypes.BIRCH, new KeyTagTreeSpecies())
                .add(TreeTypes.CRIMSON, new KeyTagTreeSpecies())
                .add(TreeTypes.DARK_OAK, new KeyTagTreeSpecies())
                .add(TreeTypes.JUNGLE, new KeyTagTreeSpecies())
                .add(TreeTypes.OAK, new KeyTagTreeSpecies())
                .add(TreeTypes.SPRUCE, new KeyTagTreeSpecies())
                .add(TreeTypes.WARPED, new KeyTagTreeSpecies())
                .build();
    }

    public static Map<ResourceKey, Material> materials() {
        return loaderBuilder(Material.class)
                .add(Materials.WOOD, new KeyTagMaterial())
                .add(Materials.STONE, new KeyTagMaterial())
                .add(Materials.LEATHER, new KeyTagMaterial())
                .add(Materials.IRON, new KeyTagMaterial())
                .add(Materials.GOLD, new KeyTagMaterial())
                .add(Materials.DIAMOND, new KeyTagMaterial())
                .add(Materials.NETHERITE, new KeyTagMaterial())
                .build();
    }
}
