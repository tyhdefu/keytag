plugins {
    `java-library`
    id("org.spongepowered.gradle.plugin") version "1.1.1"
    id("com.github.johnrengelman.shadow") version "5.2.0"
    `maven-publish`
}

repositories {
    mavenCentral()
}

val apiVersion = "0.0.0"
version = apiVersion
group = "com.tyhdefu.keytagapi"

dependencies {
    api(project(":keytagapi"))
}

sponge {
    apiVersion("8.0.0")
    plugin("keytag") {
        loader(org.spongepowered.gradle.plugin.config.PluginLoaders.JAVA_PLAIN)
        displayName("KeyTag")
        mainClass("com.tyhdefu.keytag.KeyTagPlugin")
        description("KeyTag to assist developers writing plugins!")
        links {
        }
        contributor("tyhdefu") {
            description("Lead Developer")
        }
        // Add yourself here!
        /*
        contributor("your-name-here") {
            description("Developer")
        }
         */
        dependency("spongeapi") {
            loadOrder(org.spongepowered.plugin.metadata.PluginDependency.LoadOrder.AFTER)
            optional(false)
        }
    }
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "$group"
            artifactId = "keytagapi"
            version = apiVersion

            from(components["java"])
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/27617273/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Deploy-Token"
                value = project.findProperty("gitLabPrivateToken") as String?
            }
            authentication {
                val header by registering(HttpHeaderAuthentication::class)
            }
        }
    }
}

tasks {
    build {
        dependsOn(shadowJar)
    }
}